# Python Week Of Code, Namibia 2019

Cascading Style Sheets (CSS) is a stylesheet language used to describe the presentation of a HTML document.

Today, is possible to describe how elements should be rendered on screen, on paper, in speech, or on other media using CSS.

To apply a CSS file to your HTML document,
add

```
<link href="style.css" rel="stylesheet">
```

to the `<head>` element.

Each rule is express using the following syntax:

```
selector {
    property: value;
}
```

## Selectors Cheat Sheet

- `name` selects all elements that match the given node name.
- `.class` selects all elements that have the given `class` attribute.
- `#id` selects an element based on the value of its `id` attribute.
- `[attr]` selects elements based on the value of the given attribute.
- `A B` selects nodes that are descendants of the first element.
- `A > B` selects nodes that are direct children of the first element.
- `A + B` selects adjacent siblings.
- `A ~ B` selects siblings.

## Task for Instructor

![Screenshot](images/screenshot-plain.png)

1. Add style to `h1`:

   ```
   h1 {
     color: green;
     background-color: yellow;
   }
   ```

![Screenshot](images/screenshot-h1.png)

2. Add style to the navigation bar:

   ```
   .menu {
     background-color: yellow;
   }
   .menu li {
     display: inline;
   }
   ```

![Screenshot](images/screenshot-nav.png)

## Tasks for Learners

1. Make the `<h2>` center aligned. Use `text-align: center`.
2. Make the paragraph justify. Use `text-align: justify`.
3. Mke the list italic. Use `font-style: italic`.
4. Make the list of blog titles bold. Use `font-weight: normal bolder`.